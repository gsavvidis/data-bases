CREATE VIEW `All Players` AS
SELECT footballplayer.firstName, footballplayer.lastName, footballclub.name
from footballplayer
join footballclub on footballclub.footballClubID = footballplayer.footballClubID