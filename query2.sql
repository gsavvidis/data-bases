SELECT footballclub.name
from mydb.match 
join match_between_footballclub on match.matchID = match_between_footballclub.Match_matchID 
AND match.week = match_between_footballclub.Match_week
join footballclub on match_between_footballclub.footballClubID = footballclub.footballClubID 
where match.week=13 AND (match.goalHome > 3 OR match.goalAway >3)