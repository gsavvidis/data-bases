CREATE VIEW `Injured or Doubtful Players` AS
SELECT footballplayer.firstName, footballplayer.lastName, footballclub.name
from footballplayer
join footballclub on footballclub.footballClubID = footballplayer.footballClubID
where footballplayer.healthStatus = 'Injured' OR footballplayer.healthStatus = 'Doubtful'