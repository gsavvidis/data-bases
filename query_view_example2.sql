SELECT userteam_has_footballplayer.userTeamID,ROUND(100 - SUM(footballplayer.value),1) AS 'Budget'
from userteam_has_footballplayer
join footballplayer on userteam_has_footballplayer.footballPlayerID = footballplayer.footballPlayerID
AND userteam_has_footballplayer.footballClubID = footballplayer.footballClubID
GROUP BY userteam_has_footballplayer.userTeamID