# user playing fantasy
drop user 'simpleuser'@'%';
flush privileges;
create user 'simpleuser'@'%' identified by 'simplepassword';
grant select on mydb.* to 'simpleuser'@'%';
grant select on mydb.user to 'simpleuser'@'%';
revoke select on mydb.user from 'simpleuser'@'%';
grant select on mydb.userteam to 'simpleuser'@'%';
revoke select on mydb.userteam from 'simpleuser'@'%';
grant update on mydb.userteam_has_footballplayer to 'simpleuser'@'%';
 
# employee of fantasy company
drop user 'employee'@'%';
flush privileges;
create user 'employee'@'%' identified by 'employeepass';
grant select,insert,update on mydb.* to 'employee'@'116.969.%';

#system admin
drop user 'sadmin'@'localhost';
flush privileges;
create user 'sadmin'@'localhost' identified by 'superduperpass';
grant all privileges on mydb.* to 'sadmin'@'localhost';
grant select,insert,update on mydb.* to 'sadmin'@'%';

