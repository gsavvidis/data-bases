CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `player_score` AS
    SELECT 
        `match_has_footballplayer`.`footballPlayerID` AS `footballPlayerID`,
        `match_has_footballplayer`.`footballClubID` AS `footballClubID`,
        (((((`match_has_footballplayer`.`goals` * 5) + (`match_has_footballplayer`.`assists` * 3)) + (SELECT 
                IF(`match_has_footballplayer`.`cleanSheet`,
                        4,
                        0)
            )) - (SELECT 
                IF(`match_has_footballplayer`.`yellowCard`,
                        1,
                        0)
            )) - (SELECT 
                IF(`match_has_footballplayer`.`redCard`,
                        3,
                        0)
            )) AS `Score`
    FROM
        `match_has_footballplayer`