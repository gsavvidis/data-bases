CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `user_points` AS
    SELECT 
        `user`.`firstName` AS `firstName`,
        `user`.`lastName` AS `lastName`,
        `current_week_points`.`Current_Week_Points` AS `Current_Week_Points`
    FROM
        ((`user`
        JOIN `userteam` ON ((`userteam`.`userID` = `user`.`userID`)))
        JOIN `current_week_points` ON ((`userteam`.`userTeamID` = `current_week_points`.`userTeamID`)))