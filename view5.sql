CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `current_week_points` AS
    SELECT 
        `userteam_has_footballplayer`.`userTeamID` AS `userTeamID`,
        SUM(`player_score`.`Score`) AS `Current Week Points`
    FROM
        ((`userteam_has_footballplayer`
        JOIN `footballplayer` ON (((`userteam_has_footballplayer`.`footballPlayerID` = `footballplayer`.`footballPlayerID`)
            AND (`userteam_has_footballplayer`.`footballClubID` = `footballplayer`.`footballClubID`))))
        JOIN `player_score` ON (((`player_score`.`footballPlayerID` = `footballplayer`.`footballPlayerID`)
            AND (`player_score`.`footballClubID` = `footballplayer`.`footballClubID`))))
    GROUP BY `userteam_has_footballplayer`.`userTeamID`