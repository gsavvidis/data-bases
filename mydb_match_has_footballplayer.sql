-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.5.57-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `match_has_footballplayer`
--

DROP TABLE IF EXISTS `match_has_footballplayer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_has_footballplayer` (
  `matchID` int(2) NOT NULL,
  `week` int(2) NOT NULL,
  `footballPlayerID` int(2) NOT NULL,
  `footballClubID` int(2) NOT NULL,
  `goals` int(10) unsigned DEFAULT NULL,
  `assists` int(10) unsigned DEFAULT NULL,
  `cleanSheet` bit(1) DEFAULT NULL,
  `yellowCard` bit(1) DEFAULT NULL,
  `redCard` bit(1) DEFAULT NULL,
  PRIMARY KEY (`matchID`,`week`,`footballPlayerID`,`footballClubID`),
  KEY `fk_Match_has_FootballPlayer_FootballPlayer1_idx` (`footballPlayerID`,`footballClubID`),
  KEY `fk_Match_has_FootballPlayer_Match1_idx` (`matchID`,`week`),
  CONSTRAINT `fk_Match_has_FootballPlayer_Match1` FOREIGN KEY (`matchID`, `week`) REFERENCES `match` (`matchID`, `week`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Match_has_FootballPlayer_FootballPlayer1` FOREIGN KEY (`footballPlayerID`, `footballClubID`) REFERENCES `footballplayer` (`footballPlayerID`, `footballClubID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_has_footballplayer`
--

LOCK TABLES `match_has_footballplayer` WRITE;
/*!40000 ALTER TABLE `match_has_footballplayer` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_has_footballplayer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-12 14:02:37
