CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `budget` AS
    SELECT 
        `userteam_has_footballplayer`.`userTeamID` AS `userTeamID`,
        ROUND((100 - SUM(`footballplayer`.`value`)), 1) AS `Budget`
    FROM
        (`userteam_has_footballplayer`
        JOIN `footballplayer` ON (((`userteam_has_footballplayer`.`footballPlayerID` = `footballplayer`.`footballPlayerID`)
            AND (`userteam_has_footballplayer`.`footballClubID` = `footballplayer`.`footballClubID`))))
    GROUP BY `userteam_has_footballplayer`.`userTeamID`