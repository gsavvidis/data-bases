

-- MySQL dump 10.14  Distrib 5.5.57-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.5.57-MariaDB
DROP SCHEMA IF EXISTS `mydb`;
CREATE SCHEMA `mydb`;
USE `mydb`;



/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `mydb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mydb`;

--
-- Temporary table structure for view `all players`
--

DROP TABLE IF EXISTS `all players`;
/*!50001 DROP VIEW IF EXISTS `all players`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `all players` (
  `firstName` tinyint NOT NULL,
  `lastName` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!50001 DROP VIEW IF EXISTS `budget`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `budget` (
  `userTeamID` tinyint NOT NULL,
  `Budget` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `current_week_points`
--

DROP TABLE IF EXISTS `current_week_points`;
/*!50001 DROP VIEW IF EXISTS `current_week_points`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `current_week_points` (
  `userTeamID` tinyint NOT NULL,
  `Current_Week_Points` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `footballclub`
--

DROP TABLE IF EXISTS `footballclub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footballclub` (
  `footballClubID` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL UNIQUE,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`footballClubID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footballclub`
--

LOCK TABLES `footballclub` WRITE;
/*!40000 ALTER TABLE `footballclub` DISABLE KEYS */;
INSERT INTO `footballclub` VALUES (1,'Arsenal',11),(2,'Aston Villa',300),(3,'Brighton',123),(4,'Burnley',44),(5,'Chelsea',224),(6,'Crystal Palace',142),(7,'Everton',321),(8,'Fulham',235),(9,'Leeds',262),(10,'Leicester',345);
/*!40000 ALTER TABLE `footballclub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footballplayer`
--

DROP TABLE IF EXISTS `footballplayer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footballplayer` (
  `footballPlayerID` int(2) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `value` float unsigned DEFAULT NULL,
  `position` enum('GK','DEF','MID','FW') DEFAULT NULL,
  `healthStatus` enum('Injured','Doubtful','Available') DEFAULT NULL,
  `footballClubID` int(2) NOT NULL,
  PRIMARY KEY (`footballPlayerID`,`footballClubID`),
  KEY `fk_FootballPlayer_FootballClub1_idx` (`footballClubID`),
  CONSTRAINT `fk_FootballPlayer_FootballClub1` FOREIGN KEY (`footballClubID`) REFERENCES `footballclub` (`footballClubID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footballplayer`
--

LOCK TABLES `footballplayer` WRITE;
/*!40000 ALTER TABLE `footballplayer` DISABLE KEYS */;
INSERT INTO `footballplayer` VALUES (1,'Bernd','Leno',5,'GK','Available',1),(1,'Emiliano','Martinez',4.8,'GK','Available',2),(1,'Mathew','Ryan',4.7,'GK','Injured',3),(1,'Nick','Pope',5.4,'GK','Available',4),(1,'Edouard','Mendy',5.2,'GK','Injured',5),(1,'Vicente','Guaita',4.9,'GK','Injured',6),(1,'Jordan','Pickford',4.9,'GK','Available',7),(1,'Alphonse','Areola',4.5,'GK','Available',8),(1,'Illan','Meslier',4.6,'GK','Available',9),(1,'Kasper','Schmeichel',5.5,'GK','Available',10),(2,'Hector','Bellerin',5.2,'DEF','Injured',1),(2,'Tyrone','Mings',5.3,'DEF','Available',2),(2,'Tariq','Lamptey',4.8,'DEF','Available',3),(2,'Charlie','Taylor',4.4,'DEF','Available',4),(2,'Kurt','Zouma',5.7,'DEF','Available',5),(2,'Scott','Dann',4.4,'DEF','Available',6),(2,'Michael','Kean',5.1,'DEF','Available',7),(2,'Ola','Aina',4.5,'DEF','Available',8),(2,'Stuart','Dallas',4.6,'DEF','Doubtful',9),(2,'James','Justin',4.9,'DEF','Injured',10),(3,'Pierre-Emerick','Aubameyang',11.5,'MID','Injured',1),(3,'Jack','Grealish',7.8,'MID','Doubtful',2),(3,'Solomon','March',5,'MID','Available',3),(3,'Ashley','Westwood',5.3,'MID','Available',4),(3,'Mason','Mount',6.8,'MID','Available',5),(3,'Wilfried','Zaha',7.5,'MID','Doubtful',6),(3,'James','Rodriguez',7.7,'MID','Injured',7),(3,'Ademola','Lookman',5,'MID','Injured',8),(3,'Mateusz','Klich',5.5,'MID','Available',9),(3,'James','Maddison',7.1,'MID','Available',10),(4,'Alexandre','Lacazette',8.3,'FW','Available',1),(4,'Ollie','Watkins',6.1,'FW','Available',2),(4,'Neal','Maupay',6.1,'FW','Available',3),(4,'Chris','Wood',6.2,'FW','Available',4),(4,'Timo','Werner',9.4,'FW','Available',5),(4,'Jordan','Ayew',5.7,'FW','Available',6),(4,'Dominic','Calvert-Lewin',7.9,'FW','Available',7),(4,'Bobby','Decordova-Reid',5.3,'FW','Available',8),(4,'Patrick ','Bamford',6.3,'FW','Available',9),(4,'Jamie','Vardy',10.3,'FW','Available',10);
/*!40000 ALTER TABLE `footballplayer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `injured or doubtful players`
--

DROP TABLE IF EXISTS `injured or doubtful players`;
/*!50001 DROP VIEW IF EXISTS `injured or doubtful players`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `injured or doubtful players` (
  `firstName` tinyint NOT NULL,
  `lastName` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `league`
--

DROP TABLE IF EXISTS `league`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league` (
  `leagueID` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`leagueID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league`
--

LOCK TABLES `league` WRITE;
/*!40000 ALTER TABLE `league` DISABLE KEYS */;
INSERT INTO `league` VALUES (31,'PremierLeagueProz',1251),(32,'GodBeefs',7765),(33,'Noobwars',3833),(34,'Kondaromaxies',6969),(35,'PinaColada',3323),(36,'Tripaloski',5422),(37,'PunicWarz',3789),(38,'ShadowRealm',2556),(39,'PirateGang',8832),(40,'papakia',5276);
/*!40000 ALTER TABLE `league` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `league_has_season`
--

DROP TABLE IF EXISTS `league_has_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league_has_season` (
  `leagueID` int(4) NOT NULL,
  `seasonID` int(2) NOT NULL,
  PRIMARY KEY (`leagueID`,`seasonID`),
  KEY `fk_League_has_Season_Season1_idx` (`seasonID`),
  KEY `fk_League_has_Season_League1_idx` (`leagueID`),
  CONSTRAINT `fk_League_has_Season_League1` FOREIGN KEY (`leagueID`) REFERENCES `league` (`leagueID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_League_has_Season_Season1` FOREIGN KEY (`seasonID`) REFERENCES `season` (`seasonID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league_has_season`
--

LOCK TABLES `league_has_season` WRITE;
/*!40000 ALTER TABLE `league_has_season` DISABLE KEYS */;
INSERT INTO `league_has_season` VALUES (31,11),(32,12),(33,12),(34,15),(35,14),(36,15),(37,16),(38,17),(39,17),(40,15);
/*!40000 ALTER TABLE `league_has_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `league_has_userteam`
--

DROP TABLE IF EXISTS `league_has_userteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `league_has_userteam` (
  `leagueID` int(4) NOT NULL,
  `userTeamID` int(6) NOT NULL,
  `ranking` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`leagueID`,`userTeamID`),
  KEY `fk_League_has_UserTeam_UserTeam1_idx` (`userTeamID`),
  KEY `fk_League_has_UserTeam_League1_idx` (`leagueID`),
  CONSTRAINT `fk_League_has_UserTeam_League1` FOREIGN KEY (`leagueID`) REFERENCES `league` (`leagueID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_League_has_UserTeam_UserTeam1` FOREIGN KEY (`userTeamID`) REFERENCES `userteam` (`userTeamID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `league_has_userteam`
--

LOCK TABLES `league_has_userteam` WRITE;
/*!40000 ALTER TABLE `league_has_userteam` DISABLE KEYS */;
INSERT INTO `league_has_userteam` VALUES (31,1,148),(32,2,69),(33,3,166),(34,4,27),(35,5,4),(36,6,8),(37,5,15),(38,6,16),(39,5,23),(40,8,42);
/*!40000 ALTER TABLE `league_has_userteam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match`
--

DROP TABLE IF EXISTS `match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match` (
  `matchID` int(2) NOT NULL,
  `week` int(2) NOT NULL,
  `time` char(5) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `goalHome` int(10) unsigned DEFAULT NULL,
  `goalAway` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`matchID`,`week`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match`
--

LOCK TABLES `match` WRITE;
/*!40000 ALTER TABLE `match` DISABLE KEYS */;
INSERT INTO `match` VALUES (1,13,'17:00','2020-12-21',3,3),(2,13,'15:00','2020-12-22',2,4),(3,13,'15:00','2020-12-22',1,3),(4,13,'17:00','2020-12-22',2,2),(5,13,'20:00','2020-12-22',0,3),(6,13,'20:00','2020-12-22',1,0),(7,13,'17:00','2020-12-23',0,2),(8,12,'17:00','2020-12-15',1,1),(9,12,'20:00','2020-12-15',2,5),(10,12,'20:00','2020-12-15',0,1);
/*!40000 ALTER TABLE `match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_between_footballclub`
--

DROP TABLE IF EXISTS `match_between_footballclub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_between_footballclub` (
  `Match_matchID` int(2) NOT NULL,
  `Match_week` int(2) NOT NULL,
  `footballClubID` int(2) NOT NULL,
  PRIMARY KEY (`Match_matchID`,`Match_week`,`footballClubID`),
  KEY `fk_Match_has_FootballClub_FootballClub1_idx` (`footballClubID`),
  KEY `fk_Match_has_FootballClub_Match1_idx` (`Match_matchID`,`Match_week`),
  CONSTRAINT `fk_Match_has_FootballClub_Match1` FOREIGN KEY (`Match_matchID`, `Match_week`) REFERENCES `match` (`matchID`, `week`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Match_has_FootballClub_FootballClub2` FOREIGN KEY (`footballClubID`) REFERENCES `footballclub` (`footballClubID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_between_footballclub`
--

LOCK TABLES `match_between_footballclub` WRITE;
/*!40000 ALTER TABLE `match_between_footballclub` DISABLE KEYS */;
INSERT INTO `match_between_footballclub` VALUES (1,13,1),(1,13,3),(2,13,2),(2,13,5),(3,13,8),(3,13,9),(8,12,1),(8,12,2),(9,12,3),(9,12,4);
/*!40000 ALTER TABLE `match_between_footballclub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_has_footballplayer`
--

DROP TABLE IF EXISTS `match_has_footballplayer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_has_footballplayer` (
  `matchID` int(2) NOT NULL,
  `week` int(2) NOT NULL,
  `footballPlayerID` int(2) NOT NULL,
  `footballClubID` int(2) NOT NULL,
  `goals` int(10) unsigned DEFAULT NULL,
  `assists` int(10) unsigned DEFAULT NULL,
  `cleanSheet` bit(1) DEFAULT NULL,
  `yellowCard` bit(1) DEFAULT NULL,
  `redCard` bit(1) DEFAULT NULL,
  PRIMARY KEY (`matchID`,`week`,`footballPlayerID`,`footballClubID`),
  KEY `fk_Match_has_FootballPlayer_FootballPlayer1_idx` (`footballPlayerID`,`footballClubID`),
  KEY `fk_Match_has_FootballPlayer_Match1_idx` (`matchID`,`week`),
  CONSTRAINT `fk_Match_has_FootballPlayer_Match1` FOREIGN KEY (`matchID`, `week`) REFERENCES `match` (`matchID`, `week`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Match_has_FootballPlayer_FootballPlayer1` FOREIGN KEY (`footballPlayerID`, `footballClubID`) REFERENCES `footballplayer` (`footballPlayerID`, `footballClubID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_has_footballplayer`
--

LOCK TABLES `match_has_footballplayer` WRITE;
/*!40000 ALTER TABLE `match_has_footballplayer` DISABLE KEYS */;
INSERT INTO `match_has_footballplayer` VALUES (1,13,1,1,0,1,'','',''),(1,13,2,3,1,1,'','','\0'),(1,13,3,6,2,2,'\0','\0','\0'),(2,13,1,2,0,0,'','','\0'),(2,13,3,5,0,0,'\0','\0','\0'),(2,13,4,2,3,1,'\0','\0','\0'),(3,13,2,5,0,2,'','\0','\0'),(3,13,3,9,2,1,'\0','',''),(3,13,4,8,5,0,'','\0','\0'),(3,13,4,9,1,1,'\0','\0','');
/*!40000 ALTER TABLE `match_has_footballplayer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `player_score`
--

DROP TABLE IF EXISTS `player_score`;
/*!50001 DROP VIEW IF EXISTS `player_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `player_score` (
  `footballPlayerID` tinyint NOT NULL,
  `footballClubID` tinyint NOT NULL,
  `Score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `prize`
--

DROP TABLE IF EXISTS `prize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prize` (
  `prizeID` int(2) NOT NULL AUTO_INCREMENT,
  `description` varchar(69) DEFAULT NULL,
  `category` enum('Money','Clothing','Trip','Match Ticket') DEFAULT NULL,
  `seasonID` int(2) NOT NULL,
  `userID` int(2) NOT NULL,
  PRIMARY KEY (`prizeID`,`seasonID`),
  KEY `fk_Prize_Season1_idx` (`seasonID`),
  KEY `fk_Prize_User1_idx` (`userID`),
  CONSTRAINT `fk_Prize_Season1` FOREIGN KEY (`seasonID`) REFERENCES `season` (`seasonID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Prize_User1` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prize`
--

LOCK TABLES `prize` WRITE;
/*!40000 ALTER TABLE `prize` DISABLE KEYS */;
INSERT INTO `prize` VALUES (1,'UOMO Manager Jacket','Clothing',20,3),(2,'Weekend for 2 to exotic Nevrokopi Greece','Trip',20,4),(3,'FA Cup Finals','Match Ticket',20,5),(4,'Greek National Team Cap','Clothing',20,6),(5,'Milan FC Hoodie','Clothing',20,7),(6,'Trip for 2 to Amsterdam','Trip',20,8),(7,'100$ Admiral store credit','Money',19,1),(7,'3 month Spotify Premium','Money',20,9),(8,'Greek Cup Finals Ticket','Match Ticket',19,2),(8,'50$ giftcard frm Addidas','Money',20,10);
/*!40000 ALTER TABLE `prize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `seasonID` int(2) NOT NULL AUTO_INCREMENT,
  `averageScore` int(11) DEFAULT NULL,
  PRIMARY KEY (`seasonID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (11,2355),(12,1889),(13,2005),(14,2117),(15,3109),(16,2245),(17,1234),(18,1312),(19,2709),(20,4200);
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userID` int(2) NOT NULL,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `Nationality` varchar(25) DEFAULT NULL,
  `email` varchar(35) NOT NULL UNIQUE,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Tsotsolos','Apantos','North Korea','yung_tsiant@gmail.com'),(2,'Sargios','Giovvidis','Russia','georgiss@yahoo.com'),(3,'Fastantinos','Kalis','Iran','paok1926@hotmail.com'),(4,'Pick','Napadopoulos','USA','npap@gmail.com'),(5,'Amy','Townsend','UK','amy@gmail.com'),(6,'Claude','Hammond','France','hamz@gmail.com'),(7,'Sonja','Gomez','Argentina','sonjiaG@hotmail.com'),(8,'Marc','Thornton','Australia','thorny@yahoo.com'),(9,'Juan','King','Greece','basilias@gmail.com'),(10,'Helen','Garcia','Spain','hgar@hotmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `user_points`
--

DROP TABLE IF EXISTS `user_points`;
/*!50001 DROP VIEW IF EXISTS `user_points`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_points` (
  `firstName` tinyint NOT NULL,
  `lastName` tinyint NOT NULL,
  `Current_Week_Points` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `userteam`
--

DROP TABLE IF EXISTS `userteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userteam` (
  `userTeamID` int(6) NOT NULL,
  `freeTransfers` int(11) DEFAULT NULL,
  `userID` int(2) NOT NULL,
  PRIMARY KEY (`userTeamID`),
  KEY `fk_UserTeam_User_idx` (`userID`),
  CONSTRAINT `fk_UserTeam_User` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userteam`
--

LOCK TABLES `userteam` WRITE;
/*!40000 ALTER TABLE `userteam` DISABLE KEYS */;
INSERT INTO `userteam` VALUES (1,0,1),(2,1,2),(3,2,3),(4,0,4),(5,2,5),(6,1,6),(7,2,7),(8,1,8),(9,1,9),(10,0,10);
/*!40000 ALTER TABLE `userteam` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `mydb`.`UserTeam_BEFORE_INSERT` BEFORE INSERT ON `UserTeam` FOR EACH ROW
BEGIN
	IF (NEW.freeTransfers<0 OR NEW.freeTransfers>2) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'invalid data';
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `mydb`.`UserTeam_BEFORE_UPDATE` BEFORE UPDATE ON `UserTeam` FOR EACH ROW
BEGIN
	IF (NEW.freeTransfers<0 OR NEW.freeTransfers>2) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'invalid data';
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `userteam_has_footballplayer`
--

DROP TABLE IF EXISTS `userteam_has_footballplayer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userteam_has_footballplayer` (
  `userTeamID` int(6) NOT NULL,
  `footballPlayerID` int(2) NOT NULL,
  `footballClubID` int(2) NOT NULL,
  PRIMARY KEY (`userTeamID`,`footballPlayerID`,`footballClubID`),
  KEY `fk_UserTeam_has_FootballPlayer_FootballPlayer1_idx` (`footballPlayerID`,`footballClubID`),
  KEY `fk_UserTeam_has_FootballPlayer_UserTeam1_idx` (`userTeamID`),
  CONSTRAINT `fk_Match_has_FootballClub_FootballClub1` FOREIGN KEY (`userTeamID`) REFERENCES `userteam` (`userTeamID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_FootballPlayerID` FOREIGN KEY (`footballPlayerID`, `footballClubID`) REFERENCES `footballplayer` (`footballPlayerID`, `footballClubID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userteam_has_footballplayer`
--

LOCK TABLES `userteam_has_footballplayer` WRITE;
/*!40000 ALTER TABLE `userteam_has_footballplayer` DISABLE KEYS */;
INSERT INTO `userteam_has_footballplayer` VALUES (1,3,2),(1,3,3),(3,2,2),(4,3,5),(5,1,1),(5,4,2),(6,3,2),(6,4,3),(7,2,5),(7,3,6),(9,1,7);
/*!40000 ALTER TABLE `userteam_has_footballplayer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userteam_participatesin_season`
--

DROP TABLE IF EXISTS `userteam_participatesin_season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userteam_participatesin_season` (
  `userTeamID` int(6) NOT NULL,
  `seasonID` int(2) NOT NULL,
  PRIMARY KEY (`userTeamID`,`seasonID`),
  KEY `fk_UserTeam_has_Season_Season1_idx` (`seasonID`),
  KEY `fk_UserTeam_has_Season_UserTeam1_idx` (`userTeamID`),
  CONSTRAINT `fk_UserTeam_has_Season_UserTeam1` FOREIGN KEY (`userTeamID`) REFERENCES `userteam` (`userTeamID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserTeam_has_Season_Season1` FOREIGN KEY (`seasonID`) REFERENCES `season` (`seasonID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userteam_participatesin_season`
--

LOCK TABLES `userteam_participatesin_season` WRITE;
/*!40000 ALTER TABLE `userteam_participatesin_season` DISABLE KEYS */;
INSERT INTO `userteam_participatesin_season` VALUES (1,11),(2,12),(3,12),(4,13),(5,14),(6,15),(7,16),(8,17),(9,17),(10,18);
/*!40000 ALTER TABLE `userteam_participatesin_season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mydb'
--

--
-- Current Database: `mydb`
--

USE `mydb`;

--
-- Final view structure for view `all players`
--

/*!50001 DROP TABLE IF EXISTS `all players`*/;
/*!50001 DROP VIEW IF EXISTS `all players`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `all players` AS select `footballplayer`.`firstName` AS `firstName`,`footballplayer`.`lastName` AS `lastName`,`footballclub`.`name` AS `name` from (`footballplayer` join `footballclub` on((`footballclub`.`footballClubID` = `footballplayer`.`footballClubID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `budget`
--

/*!50001 DROP TABLE IF EXISTS `budget`*/;
/*!50001 DROP VIEW IF EXISTS `budget`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `budget` AS select `userteam_has_footballplayer`.`userTeamID` AS `userTeamID`,round((100 - sum(`footballplayer`.`value`)),1) AS `Budget` from (`userteam_has_footballplayer` join `footballplayer` on(((`userteam_has_footballplayer`.`footballPlayerID` = `footballplayer`.`footballPlayerID`) and (`userteam_has_footballplayer`.`footballClubID` = `footballplayer`.`footballClubID`)))) group by `userteam_has_footballplayer`.`userTeamID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `current_week_points`
--

/*!50001 DROP TABLE IF EXISTS `current_week_points`*/;
/*!50001 DROP VIEW IF EXISTS `current_week_points`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `current_week_points` AS select `userteam_has_footballplayer`.`userTeamID` AS `userTeamID`,sum(`player_score`.`Score`) AS `Current_Week_Points` from ((`userteam_has_footballplayer` join `footballplayer` on(((`userteam_has_footballplayer`.`footballPlayerID` = `footballplayer`.`footballPlayerID`) and (`userteam_has_footballplayer`.`footballClubID` = `footballplayer`.`footballClubID`)))) join `player_score` on(((`player_score`.`footballPlayerID` = `footballplayer`.`footballPlayerID`) and (`player_score`.`footballClubID` = `footballplayer`.`footballClubID`)))) group by `userteam_has_footballplayer`.`userTeamID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `injured or doubtful players`
--

/*!50001 DROP TABLE IF EXISTS `injured or doubtful players`*/;
/*!50001 DROP VIEW IF EXISTS `injured or doubtful players`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `injured or doubtful players` AS select `footballplayer`.`firstName` AS `firstName`,`footballplayer`.`lastName` AS `lastName`,`footballclub`.`name` AS `name` from (`footballplayer` join `footballclub` on((`footballclub`.`footballClubID` = `footballplayer`.`footballClubID`))) where ((`footballplayer`.`healthStatus` = 'Injured') or (`footballplayer`.`healthStatus` = 'Doubtful')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `player_score`
--

/*!50001 DROP TABLE IF EXISTS `player_score`*/;
/*!50001 DROP VIEW IF EXISTS `player_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `player_score` AS select `match_has_footballplayer`.`footballPlayerID` AS `footballPlayerID`,`match_has_footballplayer`.`footballClubID` AS `footballClubID`,(((((`match_has_footballplayer`.`goals` * 5) + (`match_has_footballplayer`.`assists` * 3)) + (select if(`match_has_footballplayer`.`cleanSheet`,4,0))) - (select if(`match_has_footballplayer`.`yellowCard`,1,0))) - (select if(`match_has_footballplayer`.`redCard`,3,0))) AS `Score` from `match_has_footballplayer` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_points`
--

/*!50001 DROP TABLE IF EXISTS `user_points`*/;
/*!50001 DROP VIEW IF EXISTS `user_points`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_points` AS select `user`.`firstName` AS `firstName`,`user`.`lastName` AS `lastName`,`current_week_points`.`Current_Week_Points` AS `Current_Week_Points` from ((`user` join `userteam` on((`userteam`.`userID` = `user`.`userID`))) join `current_week_points` on((`userteam`.`userTeamID` = `current_week_points`.`userTeamID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-17 17:44:24
